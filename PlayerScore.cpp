#include <iostream>

class PlayerData
{
public:
    std::string name;
    int score = 0;
};

int main()
{
    int x;
    //std::string name;
    //int score;

    std::cout << "x = ";
    std::cin >> x;
    PlayerData* scores = new PlayerData[x];

    for (int i = 0; i < x; i++)
    {
        std::cout << "Name: ";
        std::cin >> scores[i].name;
        std::cout << "Score = ";
        std::cin >> scores[i].score;
    }
    std::cout << std::endl;

    for (int i = 0; i < x - 1; i++)
    {
        for (int j = 0; j < x - i - 1; j++)
        {
            if (scores[j].score < scores[j + 1].score)
            {
                int temp_s = scores[j].score;
                std::string temp_n = scores[j].name;

                scores[j].score = scores[j + 1].score;
                scores[j + 1].score = temp_s;
                scores[j].name = scores[j + 1].name;
                scores[j + 1].name = temp_n;
            }
        }

    }

    for (int i = 0; i < x; i++)
    {
        std::cout << scores[i].name << ' ' << scores[i].score << std::endl;
    }

    delete[] scores;
}